package com.example.demo.controller;

import com.example.demo.entity.Employee;
import com.example.demo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class EmployeeController {

    @Autowired
    EmployeeService service;
    @GetMapping("/get-employee/{id}")
    public Employee get(@PathVariable("id") Long id){
        return service.findEmployeeById(id);
    }
    @GetMapping("/test")
    public String test(){
        return "Tam than";
    }
    @PostMapping("/get-employee")
    public Employee save(@RequestBody Employee employee){
        return service.save(employee);
    }
}
