package com.example.demo.service;

import com.example.demo.entity.Employee;

public interface EmployeeService {
    Employee findEmployeeById(Long id);

    Employee save(Employee employee);
}
